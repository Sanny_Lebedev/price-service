$(document).ready(function(){
	
	
	 $('#AddNewProjects').click(function()  {
	 		$("#ID_PROJ").val(0);
	 		$("#inputName").val("");
			$('#myModal').modal('toggle');
	 });


   $('.User_List').click(function()  {
          var $proj = $(this).attr("id");
            $('#ProjectID').val($proj);


  $.ajax({  
                    type: "GET", 
                    url: "/otions/getprojusers",  
                    data:{"STEP": "GetUserList", "project": $proj},                  
                    cache: false,  
                    success: function(html){  
                      $("#Modal2Body").html(html);
                     }
                }); 


            $('.bs-example-modal-sm').modal('toggle');
   });



   $('.BTNHOWTO').click(function()  {
          var $proj = $(this).attr("id");
              $.ajax({  
                    type: "GET", 
                    url: "/otions/gethowto",  
                    data:{"STEP": "GetHowTo", "project": $proj},                  
                    cache: false,  
                    success: function(html){  
                      $("#Modal3Body").html(html);
                     }
                }); 


            $('.bs-example2-modal-lg').modal('toggle');
   });


$('.GolfField').on('click', '.DelProjectBtn', function () {

	 	var $tevar = $(this);
	 	var $id_proj = $(this).attr("id");
	 		 	$.ajax({  
                    type: "GET", 
                    url: "/otions/jqdelete",  
                    data:{"STEP": "DelProject", "ID_PROJ": $(this).attr("id")},                  
                    cache: false,  
                    success: function(html){  
                      if (html=="OK") {
                      	$(".panel#"+$id_proj).remove();
                      	
                      }
                     }
                }); 
	});

	 $('#SaveChangeModal').click(function(){
	 	var $ID_PROJ = $('#ID_PROJ').val();
	 	$.ajax({  
                    type: "GET", 
                    url: "/otions/jqupdate",  
                    data:{"STEP": "NewProject", "NameProject": $('#inputName').val(), "ID_PROJ": $('#ID_PROJ').val()},                  
                    cache: false,  
                    success: function(html){  
					  
                      $('#myModal').modal('toggle');

                      if ($ID_PROJ==0) {
                      			$(".GolfField").append(html);
                  			} else {
                  				location.reload();
                  			}

                        }
                        
                      
                });                             
	 });

$('.row').on('click', '.ADDFILESTR', function () {
	$('.FileArea').append("	<div class=\"form-group\"><label for=\"File[]\">File input</label><div class=\"glyphicon glyphicon-trash FLDELETE\"> </div><input type=\"file\" id=\"File[]\" name=\"File[]\"></div>");
});

$('.FileArea').on('click', '.FLDELETE', function () {
	$(this).parent().remove();
});



$('.FileArea').on('click', '.FLDELETE_EX', function () {
       $(this).parent().parent().remove();
       $.ajax({  
                    type: "GET", 
                    url: "/otions/jqdelfile",  
                    data:{"STEP": "DelBTNFile", "BTN": $(this).attr("id") },                  
                    cache: false,  
                    success: function(html){  
                      
                        }
                        
                      
                }); 
   });




$('.GolfField').on('click', '.BTNDELETE', function () {
   
   $(this).parent().parent().remove();

   $.ajax({  
                    type: "GET", 
                    url: "/otions/jqdelBTN",  
                    data:{"STEP": "DelBTN", "BTN": $(this).attr("id") },                  
                    cache: false,  
                    success: function(html){  
   
                        }
                        
                      
                }); 
});


$('.GolfField').on('click', '.Add_Button', function () {
   var $TABLID = $(this).next().next();
   $.ajax({  
                    type: "GET", 
                    url: "/otions/jqADDBTN",  
                    data:{"STEP": "ADDBTN", "PRJ": $(this).attr("id") },                  
                    cache: false,  
                    success: function(html){  
                        $($TABLID).append(html);
                        }
                        
                      
                }); 
});



$('#ModalWindows2').on('click', '#PutNewUser', function () {
   var $proj = $("#ProjectID").val();
   var $mail = $("#NewUserEmail").val();
   $.ajax({  
                    type: "GET", 
                    url: "/otions/addprojusers",  
                    data:{"STEP": "AddUserList", "project": $proj, "email": $mail},                  
                    cache: false,  
                    success: function(html){  
                      $("#Modal2Body").html(html);
                     }
                }); 
});

$('#ModalWindows2').on('click', '.DELFROMLIST', function () {
   var $attr = $(this).attr("id").split("_");

   $.ajax({  
                    type: "GET", 
                    url: "/otions/delprojusers",  
                    data:{"STEP": "DelUserList", "project": $attr[1], "user": $attr[0]},                  
                    cache: false,  
                    success: function(html){  
                      $("#Modal2Body").html(html);
                     }
                }); 
});


$('.GolfField').on('click', '.RenameProject', function () {
	 		  $("#ID_PROJ").val($(this).attr("id"));
	 		  $("#inputName").val($(this).parent().attr("alt"));
	 	      $('#myModal').modal('toggle');             
	 });
})