<?php

return [
'TERMS_OF_PUBLIC_LINE1' => 'This site is intended fot the publication of advertisements for apartments, rooms and bildings (hereinafter - the "Facilities") in the short-term lease',

'TERMS_OF_PUBLIC_LINE2' => 'All photos in the announcement to be made in checked object, match the interior of the object at the moment.',

'TERMS_OF_PUBLIC_LINE3' => 'The declaration must be checked Set the correct address of the object up to the house number.',

'TERMS_OF_PUBLIC_LINE4' => 'All ads are skipped validations before publication.',

'TERMS_OF_PUBLIC_LINE5' => 'Administration reserves the right to refuse publication of the advertisement without explanation.',

'TERMS_OF_PUBLIC_LINE6' => ' Administration has the right to unilaterally change these rules without notice.',

];

?>