<?php

return [


	'MENU_Companies' => 'Companies',
	'MENU_UserList'	=> 'Users',
	'MENU_Company'	=> 'Company',
	'MENU_USERNAME'	=> 'Name',
	'Button_Find'	=> 'Find',
	'MENU_SEARCH_PANEL' => 'Search Panel',

	'FLOOR_FEACHERS'	=>' Floors, security',
	'FLOOR_MYFLAT'		=>' Floor',
	'FLOOR_MYFLAT_OF'	=>' of ',
	'ELEVATOR'			=>' Elevator',
	'INTERCOM'			=>' Intercom',
	'CONCIERGE'			=>' Concierge',
	'SIGNALING'			=>' Alarm system',
	'CLOSED_TERRITORY'	=>' Closed territory',


	
	'Searchpanel_Check1' => 'Search in usernames',
	'Searchpanel_Check2' => 'Search in emails',
	'Searchpanel_Check3' => 'Search in company names',

	'SearchFolder_Placeholder' => 'Username, Company name or email (or part of that)',

	'MENU_RealEstates' => 'Real estates',
	'pay'			=> 'Payment Methods',
	'Address_Full' 	=> 'Your address',
	'Address_City' 	=> 'Your сity',
	'Address_Country'=> 'Your country',
	'Address_Location' => 'Location',
	'Address_MAP_SHOW' => 'Hide the map',
	'Address_MAP_SHOW_ON' => 'Show the map',

	'PANEL_PHOTOS'	=> 'Photos',
	'PANEL_PRICE'	=> 'Price and costs',


	'Real_Type'	=>	'Оbject type, main characteristics',
	'House_Single' =>  'Single family house',
	'House_Single_about' =>  'Single house for one family with a separate entrance',
	'House_Flat' =>  'Apartment',
	'House_Flat_about' =>  'Single flat in apartment house',
	'House_Single_room' =>  'Single room',
	'House_Single_room_about' =>  'Single room in the house for one or more guests',
	'House_Single_bed' =>  'Single bed',
	'House_Single_bed_about' =>  'Single bed in the room for one guest',

	'House_roomcount_text'	=> 'Rooms',
	'House_roomcount_text_1'	=> '1 room',
	'House_roomcount_text_2'	=> '2',
	'House_roomcount_text_3'	=> '3',
	'House_roomcount_text_4'	=> '4',
	'House_roomcount_text_5'	=> '5',
	'House_roomcount_text_6'	=> 'More than 5',


	'House_singlebed_count_text'	=> 'Single beds',
	'House_singlebed_count_text_0'	=> 'None',
	'House_singlebed_count_text_1'	=> '1',
	'House_singlebed_count_text_2'	=> '2',
	'House_singlebed_count_text_3'	=> '3',
	'House_singlebed_count_text_4'	=> '4',
	'House_singlebed_count_text_5'	=> '5',
	'House_singlebed_count_text_6'	=> 'More than 5',


	'House_double_count_text'	=> 'Double beds',
	'House_double_count_text_0'	=> 'None',
	'House_double_count_text_1'	=> '1',
	'House_double_count_text_2'	=> '2',
	'House_double_count_text_3'	=> '3',
	'House_double_count_text_4'	=> '4',
	'House_double_count_text_5'	=> '5',
	'House_double_count_text_6'	=> 'More than 5',

	'House_floors_text'			=> 'Floors',
	'House_floors_text_1'		=>'1',
	'House_floors_text_2'		=>'2',
	'House_floors_text_3'		=>'3',
	'House_floors_text_4'		=>'more than 3',

	'House_otherprom_text'		=>'Other',
	'house_bbq'					=>'Barbecue area',
	'house_pool'				=>'Swimming pool',
	'house_beach'				=>'The beach',
	'house_bathhouse'			=>'Bathhouse/Sauna',
	'house_playground'			=>'Playground',
	'house_garage'				=>'Garage',

	'Rooms_text'				=>'Rooms Features',
	'Rooms_type_text'			=>'Type',
	'Rooms_type_entrance_text'	=>'Type of Entrance',
	'Rooms_type_entrance_text_1'		=>'None',
	'Rooms_type_entrance_text_2'		=>'Private Entrance',
	'Rooms_type_entrance_text_3'		=>'Common Entrance',

	'Rooms_access_tothekitchen_text'	=>'Kitchen',
	'Rooms_access_tothekitchen_text_1'	=>'none',
	'Rooms_access_tothekitchen_text_2'	=>'separate',
	'Rooms_access_tothekitchen_text_3'	=>'shared',

	'Rooms_access_tothebathroom_text'	=>'Bathroom',
	'Rooms_access_tothebathroom_text_1'	=>'none',
	'Rooms_access_tothebathroom_text_2'	=>'separate',
	'Rooms_access_tothebathroom_text_3'	=>'shared',


	'flat_howmanyguests_text'	=> 'Guests in room',
	'flat_howmanyguests_text_1'	=> '1',
	'flat_howmanyguests_text_2'	=> '2',
	'flat_howmanyguests_text_3'	=> '3',
	'flat_howmanyguests_text_4'	=> '4',
	'flat_howmanyguests_text_5'	=> '5',
	'flat_howmanyguests_text_6'	=> '5+',

	'Rooms_type_text_1'			=>'None',
	'Rooms_type_text_2'			=>'Through room',
	'Rooms_type_text_3'			=>'Single room',


	'House_Features'			=> 'Main Features',
	'Appartment_Features'		=> 'Main Features',

	'Appartment_multi_level'	=>'Multi-level apartment',
	'Appartment_studio'			=>'Studio',
	'Rooms_and_Beds'			=>'Rooms and beds',

	'PRICE_PER_DAY'				=>'Price per day',
	'PRICE_PER_HOUR'			=>'Price per hour',
	'PRICE_PER_MONTH'			=>'Price per month',
	'PRICE_PER_NIGHT'			=>'Price per night',

	'ESTATE_TIME_TEXT'			=>'Minimum rental period',
	'ESTATE_TIME_1'				=>'Days',
	'ESTATE_TIME_2'				=>'Hours',
	'ESTATE_TIME_3'				=>'Month',

	'PERIOD_PRICE_TEXT'			=>'Add price',
	'PRICE_TEXT_COMMENT'		=>'Attention, you must specify the price of the whole object (without any division per person)',

	'PANEL_PAYMENT_METHOD'		=>'Payment method',
	'PAYMENT_METHOD_MAINPANEL'	=>'',
	'PAYMENT_CREDIT_CARDS'		=>'Credit cards',
	'PAYMENT_CASH'				=>'Cash',
	'PAYMENT_BANK_TRANSFER'		=>'Bank transfer',
	'PAYMENT_EMONEY'			=>'E-money',
	'PAYMENT_ABOUT'				=>'Payment remark',


	'PANEL_Reservation_and_deposit'	=>'Reservation and deposit',
	'RESERVATION_ABOUT'			=> 'Reservation',
	'DEPOSIT_ABOUT'				=> 'Deposit',
	'DISCOUNTS_ABOUT'			=> 'Conditions for discounts',
	'RESERVATION_REPDOC'		=> 'Provide reporting documents for business travelers',

	'PANEL_INET'				=> 'Internet/media',
	'INET_TYPE'					=> 'Internet',
	'INET_TYPE_1'				=> 'not specified',
	'INET_TYPE_2'				=> 'none',
	'INET_TYPE_3'				=> 'limited',
	'INET_TYPE_4'				=> 'unlimited',
	'IS_PC'						=> 'Computer/laptop available',
	'TV_TYPE'					=> 'TV',
	'TV_TYPE_1'					=> 'not specified',
	'TV_TYPE_2'					=> 'none',
	'TV_TYPE_3'					=> 'cable',
	'TV_TYPE_4'					=> 'digital',
	'TV_TYPE_5'					=> 'satellite',
	'TV_TYPE_6'					=> 'IP',
	'HOME_CINEMA'				=> 'Home cinema',
	'GAME_CONSOLE'				=> 'Game console',


	'PANEL_Cooking_cleaning_and_comfort'	=> 'Cooking, cleaning and comfort',
	'KITCHEN_PART'				=> 'Kitchen',
	'KITCHEN_GAS_STOVE'			=> 'Gas stove',
	'KITCHEN_ELECTRIC_STOVE'	=> 'Electric stove',
	'KITCHEN_OVEN'				=> 'Oven',
	'KITCHEN_REFRIGERATION'		=> 'Refrigeration',
	'KITCHEN_FREEZER'			=> 'Freezer',
	'KITCHEN_MICROWAVE'			=> 'Microwave',
	'KITCHEN_TOASTER'			=> 'Toaster',
	'KITCHEN_COFFEE_MACHINE'	=> 'Coffee Machine',
	'KITCHEN_COFFEE_MAKER'		=> 'Coffee maker',
	'KITCHEN_KETTLE'			=> 'Kettle',

	'CLEANING_PART'				=> 'Cleaning',
	'CLEANING_DISHWASHER'		=> 'Dishwasher',
	'CLEANING_WASHING_MACHINE'	=> 'Washing machine',
	'CLEANING_IRON'				=> 'Iron',
	'CLEANING_VACUUM_CLEANER'	=> 'Vacuum cleaner',
	'CLEANING_CLOSER_DRYER'		=> 'Closer dryer',

	'COMFORT_PART'				=> 'Comfort',
	'COMFORT_AIR_CONDITION'		=> 'Air condition',
	'COMFORT_IONIZER'			=> 'Ionizer',
	'COMFORT_AIR_HUMIDIFIER'	=> 'Air humidifier',
	'COMFORT_WATER_FILTER'		=> 'Water filter',
	'COMFORT_WATER_HEATER'		=> 'Water heater',

	'PANEL_WC'					=> 'The restroom',
	'WC_TYPE'					=>  'The restroom\'s type',
	'WC_TYPE_1'					=> 'not specified', 
	'WC_TYPE_2'					=> 'combined',
	'WC_TYPE_3'					=> 'separated',

	'RESTROOM_BATHTYPE'			=> '',
	'RESTROOM_SHOWER_CABINS'	=> 'Shower',
	'RESTROOM_BATH'				=> 'Bath',
	'RESTROOM_JACUZZI'			=> 'Jacuzzi',

	'RESTROOM_BATHTYPE1'		=> '',
	'RESTROOM_BIDET'			=> 'Bidet',
	'RESTROOM_WARM_FLOOR'		=> 'Warm floor',
	'RESTROOM_HAIRDRYER'		=> 'Hairdryer',

	'PANEL_TOR'					=> 'Terms of residence',
	'PETS'						=> 'PETS',
	'PETS_1'					=> 'not specified',
	'PETS_2'					=> 'not allowed',
	'PETS_3'					=> 'allowed',

	'SMOKING'					=> 'Smoking',
	'SMOKING_1'					=> 'not specified',
	'SMOKING_2'					=> 'not allowed',
	'SMOKING_3'					=> 'allowed',

	'PARTIES'					=> 'Parties',
	'PARTIES_1'					=> 'not specified',
	'PARTIES_2'					=> 'not allowed',
	'PARTIES_3'					=> 'allowed',

	'PANEL_DESCRIPTIONS'		=> 'Descriptions and contacts',
	'DESCRIPTIONS_SHORT'		=> 'Short descriptions',
	'DESCRIPTIONS_SHORT_HOLDER'	=> 'Your short descriptions',

	'DESCRIPTIONS_LONG'			=> 'Long descriptions',
	'DESCRIPTIONS_COMPANY_NAME'	=> 'Name or company name',
	'DESCRIPTIONS_COMPANY_NAME_HOLDER'	 => 'Name',
	'DESCRIPTIONS_PHONES'		=> 'Phones',
	'DESCRIPTIONS_PUBLIC_EMAIL' => 'Public email',
	'DESCRIPTIONS_ICQ'			=> 'ICQ',
	'DESCRIPTIONS_SKYPE'		=> 'Skype',
	'DESCRIPTIONS_FB_LINK'		=> 'Your FB link'
	,

	'LINK_RULES_AND_TERMS'		=> 'By clicking on "post" button, you agree to the rules of publication',
	'TITLE_RULES_AND_TERMS'		=> 'Rules and terms',

	'PANEL_SEND_BUTTON'			=> 'Post new adv',




];