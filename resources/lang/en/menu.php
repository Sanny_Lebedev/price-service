<?php

return [


    'profile_profile' => 'Profile',
    'profile_messages' => 'Messages',
    'profile_home' => 'Home',
    'profile_family'	=> 'Family',
    'profile_name'	=> 'Name',
    'profile_second_name'	=> 'Second name',

    'profile_sex'	=> 'Sex',
    'profile_male'	=> 'Male',
	'profile_female'	=> 'Female',
	'profile_birthday'	=> 'Birthday',

	'profile_phone'	=> 'Phone',	
	'profile_ICQ'	=> 'ICQ',
	'profile_SKYPE'	=> 'SKYPE',

	'profile_VK_LINK'	=> 'VK profile (link)',
	'profile_FB_LINK'	=> 'FB Profile (link)',
	'profile_SITE_LINK'	=> 'Your website',
	'profile_About'	=> 'About',


    'menu_languages'     => 'Languages',
    'menu_languages_rus' => 'Russian',
    'menu_languages_eng' => 'English',

    'ru' => 'Russian',
    'en' => 'English',

    'Button_SAVE' => 'Save',
    'profile_ads' => 'Publications',
    'profile_ads_new'	=> 'New publication',
    'profile_home'  => 'Home',



    'adv_name'  => 'Name',
    'adv_phone'  => 'Phone',
    'adv_skype'  => 'Skype',
    'adv_ICQ'  => 'ICQ',
    'adv_email'  => 'E-mail',
    'adv_VK_Profile'  => 'VK profile',
    'adv_FB_Profile'  => 'FB profile',
    'adv_About'  => 'About',
    'adv_Orientation'  => 'Sexual orientation',
    'adv_Orientation_gettero'  => 'Getterosexual',
    'adv_Orientation_gomo'  => 'Homosexual',
    'adv_Orientation_bi'  => 'Bi',
    'adv_Age'  => 'Age',
    'adv_Height'  => 'Height',
    'adv_Weight'  => 'Weight',
    'adv_Bust'  => 'Bust',
    'adv_Eye_color'  => 'Eye color',
    'adv_Hair_color'  => 'Hair color',
    'adv_Country'  => 'Country',
    'adv_City'  => 'City',
    'adv_Address'  => 'Address',
    'adv_X_COORD'  => 'X_COORD',
    'adv_Y_COORD'  => 'Y_COORD',


];
