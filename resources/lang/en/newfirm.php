<?php

return [


	'firmtable' => 'Firms',
	'NewFirmButton' => 'New Firm',
	'Firm_name' => 'Name',
	'Firm_Adress' => 'Adress',
	'FIRM_CITY' => 'CITY',
	'INN' => 'INN',
	'KPP' => 'KPP',
	'OGRN' => 'OGRN',
	'rschet' => 'R/Schet',
	'kschet' => 'K/Schet',
	'BIK' => 'BIK',
	'BANK' => 'BANK',
	'Phone' => 'Phone',
	'Email' => 'E-mail',
	'Skype' => 'Skype',
	'GroupType' => 'Group',
	'FirmAbout' => 'About',
	'FirmActive_YES' => 'Active',
	'FirmActive_NO' => 'Disable',
	'FirmActive' => 'Active',
	'GroupType_1'=>'Administrators',
	'GroupType_2'=>'Sellers',
	'GroupType_3'=>'buyer',
	'Cancel'=>'Cancel',
	'DeleteFirm'=>'Delete this company',
	'Company'=>'Company',
	'NewFirmButton'=>'Add new company',

];