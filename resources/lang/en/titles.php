<?php

return [


	'firmtable' => 'Firms',
	'NewFirmButton' => 'New Firm',
	'Firm_name' => 'Name',
	'Firm_Adress' => 'Adress',
	'FIRM_CITY' => 'CITY',
	'INN' => 'INN',
	'KPP' => 'KPP',
	'OGRN' => 'OGRN',
	'rschet' => 'R/Schet',
	'kschet' => 'K/Schet',
	'BIK' => 'BIK',
	'BANK' => 'BANK',
	'Phone' => 'Phone',
	'Email' => 'E-mail',
	'Skype' => 'Skype',
	'myrealestateroom' => 'My real estates',
	'myrealestateroom_new' => 'New real estate',


];
