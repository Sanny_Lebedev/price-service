<?php

return [

'workgroup_name'        	=>  'Name of Service List',
'workgroup_vals'			=>	'Сurrency is',
'Add_Service_List'      	=>  'Add new Service List', 
'workgroup_cost_plan'   	=>  'Cost by plan',
'workgroup_cost_calc'   	=>  'Cost by calculate',

'workgroup_period_cost'		=>	'Cost',
'workgroup_period_30mins'	=>	'for 30 minuts',
'workgroup_period_hour'		=>	'for 1 hour',
'workgroup_period_2hour'	=>	'for 2 hours',
'workgroup_period_3hour'	=>	'for 3 hours',
'workgroup_period_night'	=>	'for night',

'workgroup_delete_list'		=>	'Delete list of services',

'SPA_SALOON_TYPE'			=>	'Spa',
'SPA_SALOON_TYPE_ABOUT'		=>	'Spa treatments, sauna',

'MASSAGE_SALOON_TYPE'		=> 'Massage',
'MASSAGE_SALOON_TYPE_ABOUT'	=> 'Erotic massage',

'BORDEL_SALOON_TYPE'		=> 'BORDEL', 
'BORDEL_SALOON_TYPE_ABOUT'	=> 'Sex-service from the salon', 

'IND_SALOON_TYPE'			=> 'Private worker', 
'IND_SALOON_TYPE_ABOUT'		=> 'Sex-service from the privat worker', 

'day_monday'	=> 'Mon.',
'day_tuesday'	=> 'Tue.',
'day_wednesday'	=> 'Wed.',
'day_thursday'	=> 'Thu.',
'day_friday'	=> 'Fri.',
'day_saturday'	=> 'Sat.',
'day_sunday'	=> 'Sun.',
'Work_days'		=>	'Work days:',
'Time_Range'	=>	'Time range:',
'Bust_Size_Number'	=>	'Your size №',

'orient_normal'		=>	'Hetero',
'orient_lesbo'		=>	'Homo',
'orient_gey'		=>	'Homo',
'orient_biman'		=>	'Bi',
'orient_biwoman'	=>	'Bi',

'Your_Age_YEARS'	=>	'years',
'Your_Age'			=>	'Your age:',

'Your_Weight'		=>	'Your weight:',
'Your_Weight_Metrics'	=>	'kg.',

'Your_Height'			=>	'',
'Your_Height_Metrics'	=>	'sm.',
'List_of_Service'		=> 'List of Services',
'List_of_Service_More'	=> 'More information',
'List_of_Service_About_Title'		=> 'About',
'List_of_Service_About_Body'		=> 'You can write some information about your service here.',

'Girls_Delete'		=> 'Delete this publication',
'Girls_Config'		=> 'Configuration',
];
