<?php

return [


'workgroup_name'    		=>  'Наименование списка услуг',
'workgroup_vals'			=>	'Валюта: ',
'Add_Service_List' 			=>  'Добавить новый список услуг', 
'workgroup_cost_plan'   	=>  'Цена на услугу (продажа)',
'workgroup_cost_calc'   	=>  'Расчетная стоимость',
'workgroup_period_cost'		=>	'Цена',
'workgroup_period_30mins'	=>	'за 30 минут',
'workgroup_period_hour'		=>	'за 1 час',
'workgroup_period_2hour'	=>	'за 2 часa',
'workgroup_period_3hour'	=>	'за 3 часa',
'workgroup_period_night'	=>	'за ночь',


'workgroup_delete_list'		=>	'Удалить список услуг',


'SPA_SALOON_TYPE'			=>	'СПА',
'SPA_SALOON_TYPE_ABOUT'		=>	'Спа-салон, сауна, массаж',

'MASSAGE_SALOON_TYPE'		=> 'Массаж',
'MASSAGE_SALOON_TYPE_ABOUT'	=> 'Все виды эротического массажа',

'BORDEL_SALOON_TYPE'		=> 'Салон', 
'BORDEL_SALOON_TYPE_ABOUT'	=> 'Салон интимных услуг', 

'IND_SALOON_TYPE'			=> 'Индивидуалки', 
'IND_SALOON_TYPE_ABOUT'		=> 'Девушки, работающие индивидуально', 

'day_monday'	=> 'Пн.',
'day_tuesday'	=> 'Вт.',
'day_wednesday'	=> 'Ср.',
'day_thursday'	=> 'Чт.',
'day_friday'	=> 'Пт.',
'day_saturday'	=> 'Сб.',
'day_sunday'	=> 'Вс.',
'Work_days'		=>	'Рабочие дни:',
'Time_Range'	=>	'Время работы:',

'Bust_Size_Number'	=>	'Ваш размер №',

'orient_normal'		=>	'Гетеро',
'orient_lesbo'		=>	'Гомо',
'orient_gey'		=>	'Гомо',
'orient_biman'		=>	'Би',
'orient_biwoman'	=>	'Би',

'Your_Age_YEARS'	=>	'лет',
'Your_Age'			=>	'Ваш возраст:',

'Your_Weight'		=>	'Ваш вес:',
'Your_Weight_Metrics'	=>	'кг.',

'Your_Height'			=>	'',
'Your_Height_Metrics'	=>	'см.',

'List_of_Service'		=> 'Список услуг',
'List_of_Service_More'	=> 'Подробное описание',
'List_of_Service_About_Title'		=> 'Подсказка',
'List_of_Service_About_Body'		=> 'Вы можете добавить более подробную информацию о предлагаемых услугах',

'Girls_Delete'		=> 'Удалить это объявление',
'Girls_Config'		=>	'Настройка объявления',

];
