<?php

return [

    'profile_profile' => 'Профиль',
    'profile_messages' => 'Сообщения',
    'profile_home' => 'Страница',
    'profile_family'	=> 'Фамилия',
    'profile_name'	=> 'Имя',
    'profile_second_name'	=> 'Отчество',


    'profile_sex'	=> 'Ваш пол',
    'profile_male'	=> 'Мужской',
	'profile_female'	=> 'Женский',
	'profile_birthday'	=> 'Дата рождения',

	'profile_phone'	=> 'Телефон',	
	'profile_ICQ'	=> 'ICQ',
	'profile_SKYPE'	=> 'SKYPE',

	'profile_VK_LINK'	=> 'Профиль ВКонтакте',
	'profile_FB_LINK'	=> 'Профиль FaceBook',
	'profile_SITE_LINK'	=> 'Ваш сайт',
	'profile_About'	=> 'О себе',

	'menu_languages'	 => 'Язык',
	'menu_languages_rus' => 'Русский',
	'menu_languages_eng' => 'English',
    
	'ru' => 'Russian',
	'en' => 'English',

    'Button_SAVE' => 'Сохранить изменения',
    'profile_ads' => 'Объявления',
    'profile_ads_new'	=> 'Новое объявление',
    'profile_home'  => 'Главная',


        'adv_name'  => 'Name',
    'adv_phone'  => 'Phone',
    'adv_skype'  => 'Skype',
    'adv_ICQ'  => 'ICQ',
    'adv_email'  => 'E-mail',
    'adv_VK_Profile'  => 'VK profile',
    'adv_FB_Profile'  => 'FB profile',
    'adv_About'  => 'About',
    'adv_Orientation'  => 'Sexual orientation',
    'adv_Orientation_gettero'  => 'Getterosexual',
    'adv_Orientation_gomo'  => 'Homosexual',
    'adv_Orientation_bi'  => 'Bi',
    'adv_Age'  => 'Age',
    'adv_Height'  => 'Height',
    'adv_Weight'  => 'Weight',
    'adv_Bust'  => 'Bust',
    'adv_Eye_color'  => 'Eye color',
    'adv_Hair_color'  => 'Hair color',
    'adv_Country'  => 'Country',
    'adv_City'  => 'City',
    'adv_Address'  => 'Address',
    'adv_X_COORD'  => 'X_COORD',
    'adv_Y_COORD'  => 'Y_COORD',
];