@extends('layouts.welcome')

        @section('DOP_CSS')  
        <link rel="stylesheet" href="/css/main.css">
        @endsection

@section('Title')
Отправка КП и прайсов с сайта на почту клиентов - Сервис от Agency911.org
@endsection

@section('content')

<div class="container-fluid">
  <div class="row">
    <div class="col-sm-10 col-sm-offset-1">
      <h1>Информация о проектах</h1>
      
@if (session('success'))
<div class="alert alert-success alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<strong>SUCCESS</strong> {{ session('success') }}
</div>
@endif

  @if (isset($Users_Proj_List))
   @foreach ($Users_Proj_List as $Proj)
    <div class="proj_name"><h3>Проект: {{$Proj->About}} (№ {{$Proj->ID_PROJECT}}) </h3></div>
    <div>
    <div class="proj_btn_stat"> <a href='/stat/{{$Proj->ID_PROJECT}}'> Статистика запросов</a> </div>
    <div class="proj_btn_stat"> <a href='/adr/{{$Proj->ID_PROJECT}}'>Набор адресов </a></div>
	</div>
   @endforeach
  @endif


</div>
</div>
</div>

@endsection

@section('JS_CODE')
         
@endsection
