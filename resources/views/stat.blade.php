<!-- resources/views/auth/password.blade.php -->
@extends('layouts.welcome')

        @section('DOP_CSS')  
        <link rel="stylesheet" href="/css/main.css">
        @endsection

@section('content')

<div class="container-fluid">
  <div class="row">
    <div class="col-sm-10 col-sm-offset-1">
      <h1>Информация о проектах</h1>
      
@if (session('success'))
<div class="alert alert-success alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<strong>SUCCESS</strong> {{ session('success') }}
</div>
@endif

  @if (isset($StatSends))
  <div class="proj_name"><h3>Проект: {{$Project_name}} (№ {{$Project_id}}) </h3></div>
  <table class="table table-hover table-striped">
  <tr>
  <th>Отправлено</th>
  <th>Имя</th>
  <th>E-mail</th>
  <th>Кнопка</th>
  </tr>
   @foreach ($StatSends as $Stat)
   <tr>
   <td>{{$Stat->created_at}}</td>
   <td>{{$Stat->UserName}}</td>
   <td>{{$Stat->Email}}</td>
   <td>{{$Stat->BTN_Caption}}</td>
   </tr>
   @endforeach
   </table>
   {!! $StatSends->render() !!}
  @endif


</div>
</div>
</div>

@endsection

@section('JS_CODE')
         
@endsection
