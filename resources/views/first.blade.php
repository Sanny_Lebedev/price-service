@extends('layouts.welcome')

        @section('DOP_CSS')  
        <link rel="stylesheet" href="/css/main.css">
        @endsection

@section('Title')
Отправка КП и прайсов с сайта на почту клиентов - Сервис от Agency911.org
@endsection

@section('content')

<div class="container-fluid">
  <div class="row">
    <div class="col-sm-10 col-sm-offset-1">
      <h1>Сервис "получить прайс на почту"</h1>
      
<div class="infopanel1">
  <p>У Вас есть сайт и Вы что-то продаете? Наш сервис поможет Вам добавить на Ваш сайт форму запроса прайс-листа или коммерческого предложения. Ваши посетители получат данные на email, Вы фиксируете электронные адреса запросивших. </p> 


</div>

<div class="infopanel1">
<p><strong>Все работает очень просто:</strong></p>
  <ul>
   <li>Вы регистрируетесь в системе</li>
   <li>Вы заполняете данные Вашей электронной почты (через которую осуществляется отправка предложений для Ваших клиентов)</li>
   <li>Вы настраиваете вид кнопки-запроса, формируете текст письма и загружаете файлы для вложений</li>
   <li>Вы редактируете макет сайта (как именно - мы Вам расскажем, но это очень просто, всего лишь 4 строки)</li>
   <li>Мы отправляем письмо по запросу с Вашего сайта через Ваш адрес</li>
   <li>Мы сохраняем статистику и фиксируем электронный адрес запросившего</li>
   <li>Мы предоставляем многопользовательский доступ к статистике (Вы сами открываете доступ для зарегистрированных в системе пользователей)</li>
  </ul>
  <p><strong>В ближайших планах - рассылка по базе адресов Ваших клиентов</strong></p>
</div>

<div class="mainbuttondiv">
<a href="{{ url('/auth/login') }}"><div type="button" class="btn btn-info BTN_MAIN1">Войти</div></a>
<a href="{{ url('/auth/register') }}"><div type="button" class="btn btn-info BTN_MAIN1">Регистрация</div></a>
</div>


</div>
</div>
</div>

@endsection

@section('JS_CODE')
         
@endsection
