<h2>Инструкция по установке кнопки</h2>
<p>Для корректной работы сервиса Вам необходимо внести ряд изменений в Ваш макет</p>
<div>1. Прежде всего, Вам необходимо подключить Bootstrap:
<pre>
{{ '<link rel="stylesheet" href="/Btstp/css/bootstrap.min.css"> ' }}
</pre>

</div>

<div>2. В макете Вам необходимо выбрать место для размещения кнопки и добивить данный код размещения:
<pre>
{{ $LineContr }}
</pre>
<div>3. В нижней части макета, до {{'</body>'}}, необходимо добавить блок размещения окна:
<pre>
{{ '<div id="SendPriceModal"></div>' }}
</pre>

<div>4. В конец макета, до {{'</body>'}}, Вам необходимо добавить вызов скриптов bootstrap и нашего сервиса:
<pre>
{{ '<script src="http://service.agency911.org/js/ForPriceToEmail.js"></script>' }}
{{ '<script src="http://Ваш домен/Путь к Bootstrap/bootstrap/js/bootstrap.min.js"></script>' }}
</pre>

<div><strong>Не забудьте корректно настроить кнопки, а именно:</strong>
<div>1. Наименование</div>
<div>2. Стиль оформления (css)</div>
<div>3. Заголовок и текст письма</div>
<div>4. Файлы вложения</div>
</div>

</div>