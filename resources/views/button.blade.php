<!-- resources/views/auth/password.blade.php -->
@extends('layouts.welcome')

@section('Title')
Отправка КП и прайсов с сайта на почту клиентов - Сервис от Agency911.org
@endsection

@section('content')

<div class="container-fluid">
  <div class="row">
    <div class="col-sm-10 col-sm-offset-1">
      <h2>Настройка Кнопки для проекта</h2>
      
@if (session('success'))
<div class="alert alert-success alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<strong>SUCCESS</strong> {{ session('success') }}
</div>
@endif

  

<form class="form-horizontal" method="POST" enctype="multipart/form-data">
  <div class="form-group"> 
{!! csrf_field() !!}
    <label for="BTN_Caption" class="col-sm-2 control-label">Наименование Кнопки</label>
    <div class="col-sm-10">
    <input type="text" class="form-control" id="BTN_Caption" name="BTN_Caption" placeholder="Наименование кнопки" value="{{$Button->BTN_Caption}}">
    </div>
    </div>

	<div class="form-group"> 
    <label for="BTN_Style" class="col-sm-2 control-label">Стиль Кнопки</label>
    <div class="col-sm-10">
    <textarea class="form-control" id="BTN_Style" name="BTN_Style"  rows="5">{{$Button->BTN_Style}}</textarea>
    </div>
    </div>

	<div class="form-group"> 
     <label for="Model_Caption" class="col-sm-2 control-label">Наименование Окна</label>
    <div class="col-sm-10">
    <input type="text" class="form-control" id="Model_Caption" name="Model_Caption" placeholder="Наименование модального окна" value="{{$Button->Model_Caption}}">
    </div>
    </div>
	<div class="form-group"> 
    <label for="Model_Style" class="col-sm-2 control-label">Стиль Окна</label>
    <div class="col-sm-10">
    <textarea class="form-control" id="Model_Style" name="Model_Style"  rows="5">{{$Button->Model_Style}}</textarea>
    </div>
    </div>



	<div class="form-group"> 
    <label for="LETTER_Caption" class="col-sm-2 control-label">Заголовок письма</label>
    <div class="col-sm-10">
    <input type="text" class="form-control" id="LETTER_Caption" name="LETTER_Caption" placeholder="Наименование модального окна" value="{{$Actions->LETTER_Caption}}">
    </div>
    </div>

    <div class="form-group"> 
    <label for="LETTER_BODY" class="col-sm-2 control-label">Тело письма</label>
    <div class="col-sm-10">
    <textarea class="form-control" id="LETTER_BODY" name="LETTER_BODY"  rows="5">{{$Actions->LETTER_BODY}}</textarea>
    </div>
    </div>

     <div class="form-group"> 
    <label for="SMS_TEXT" class="col-sm-2 control-label">Тело SMS</label>
    <div class="col-sm-10">
    <textarea class="form-control" id="SMS_TEXT" name="SMS_TEXT"  rows="5">{{$Actions->SMS_TEXT}}</textarea>
    </div>
    </div>

     <button type="button" class="btn btn-primary glyphicon glyphicon-plus ADDFILESTR">Добавить файл</button>
    <div class="FileArea">

    
    @foreach ($BFILES as $File)

    <div class="form-group">
    <label for="Files[]" class="col-sm-2 control-label"></label>
    
    <div class="col-sm-8">
    <div class="">{{$File->FILENAME}}</div>
   </div>
    <div class="col-sm-2">
    <div class="glyphicon glyphicon-trash FLDELETE_EX" id="{{$ID_PROJECT}}_#_{{$ID_BUT}}_#_{{$File->FILENAME}}"> </div>
   </div>

    </div>

    @endforeach



	<div class="form-group">
    <label for="File[]">File input</label>
    <div class="glyphicon glyphicon-trash FLDELETE"> </div>
    <input type="file" id="File[]" name="File[]">

  	</div>


    </div>
    
    

     <div class="form-group"> 
    <label class="col-sm-2 control-label"></label>
    <div class="col-sm-10">
    <button type="submit" style="width: 100%;" class="btn btn-primary">Сохранить данные</button>
    </div>
    </div>



</form>

</div>
</div>
</div>

@endsection

@section('JS_CODE')
         {!! Html::script('js/JSproj.js') !!}
@endsection
