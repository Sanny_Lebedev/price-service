 <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">{{$Caption}}</h4>
      </div>
      
      <div class="modal-body form-horizontal">
        <div class="form-group">
    	<label for="inputName3" class="col-sm-3 control-label">Ваше имя</label>
    	<div class="col-sm-9">
      	<input type="text" class="form-control" id="inputName3" placeholder="ФИО">
    	</div>
  		</div>

  		<div class="form-group">
    	<label for="inputEmail3" class="col-sm-3 control-label">Email</label>
    	<div class="col-sm-9">
      	<input type="email" class="form-control" id="inputEmail3" placeholder="Email">
    	</div>
  		</div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-primary" id="SendModalBTN">Отправить</button>
      </div>

    </div>
  </div>
</div>