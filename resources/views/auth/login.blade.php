<!-- resources/views/auth/login.blade.php -->
@extends('layouts.welcome')

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-4 col-sm-offset-4">
      <h2>Авторизация</h2>

      <form method="post" action="{{ url('/auth/login') }}">
        {!! csrf_field() !!}

        <div class="form-group">
          <label for="exampleInputEmail1">E-mail</label>
          <input type="email" class="form-control" id="exampleInputEmail1" placeholder="E-mail" name="email" value="{{ old('email') }}" required>
        </div>

        <div class="form-group">
          <label for="exampleInputPassword1">Пароль</label>
          <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password" required>
          <input type="hidden" class="form-control" name="provider" value="local" required>
        </div>

        <div class="checkbox">
            <label>
              <input type="checkbox" name="remember"> Запомнить
            </label>
        </div>

        <div class="form-group">
          <button class="btn btn-lg btn-primary btn-block" type="submit">Войти</button>
        </div>

        <p class="text-center">
          <a class="btn btn-link" href="{{ url('/password/email') }}">Забыли пароль?</a>
        </p>

        @if (count($errors) > 0)
          <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif

      </form>
    </div>
  </div>
</div>
@endsection