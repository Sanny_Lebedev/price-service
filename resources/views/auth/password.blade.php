<!-- resources/views/auth/password.blade.php -->
@extends('layouts.welcome')

@section('content')

<div class="container-fluid">
  <div class="row">
    <div class="col-sm-4 col-sm-offset-4">
      <h2>Reset password?</h2>

@if (session('status'))
      <div class="alert alert-success">
             {{ session('status') }}
      </div>
    @endif

    @if (count($errors) > 0)
       <div class="alert alert-danger">
       <strong>Whoops!</strong> There were some problems with your input.<br><br>
       <ul>
           @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
           @endforeach
        </ul>
        </div>
      @endif


      <form method="post" action="{{ url('/password/email') }}">
        {!! csrf_field() !!}

    
 <div class="form-group">
          <label for="email">Email</label>
          <input type="email" class="form-control" id="email" placeholder="email" name="email" value="{{ old('email') }}" required>
          <input type="hidden" class="form-control" name="provider" value="local" required>
        </div>


        <div class="form-group">
          <button class="btn btn-lg btn-primary btn-block" type="submit">Send Password Reset Link</button>
        </div>


</form>
</div>
</div>
</div>
@endsection