<!-- resources/views/auth/password.blade.php -->
@extends('layouts.welcome')

@section('content')

<div class="container-fluid">
  <div class="row">
    <div class="col-sm-4 col-sm-offset-4">
      <h2>Your new password</h2>






      <form method="post" action="{{ url('/password/reset') }}">
        {!! csrf_field() !!}
 <input type="hidden" name="token" value="{{ $token }}">
    
 <div class="form-group">
          <label for="email">Email</label>
          <input type="email" class="form-control" id="email" placeholder="email" name="email" value="{{ old('email') }}" required>
        </div>

 <div class="form-group">
          <label for="password">Пароль</label>
          <input type="password" class="form-control" id="password" name="password"  required>
        </div>

 <div class="form-group">
          <label for="password_confirmation">Пароль</label>
          <input type="password" class="form-control" id="password_confirmation" name="password_confirmation"  required>
          <input type="hidden" class="form-control" name="provider" value="local" required>
        </div>

        <div class="form-group">
          <button class="btn btn-lg btn-primary btn-block" type="submit">Сброс пароля</button>
        </div>


</form>
</div>
</div>
</div>
@endsection