<!-- resources/views/auth/register.blade.php -->
@extends('layouts.welcome')

@section('content')


@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif

<div class="container-fluid">
  <div class="row">
    <div class="col-sm-4 col-sm-offset-4">
      <h2>Please sign in</h2>

      <form method="post" action="{{ url('/auth/register') }}">
        {!! csrf_field() !!}



 <div class="form-group">
          <label for="name">Имя</label>
          <input type="text" class="form-control" id="name" placeholder="name" name="name" value="{{ old('name') }}" required>
        </div>
 <div class="form-group">
          <label for="email">E-mail</label>
          <input type="text" class="form-control" id="email" placeholder="E-mail" name="email" value="{{ old('email') }}" required>
        </div>

 <div class="form-group">
          <label for="password">Пароль</label>
          <input type="password" class="form-control" id="password" name="password" required>
        </div>

 <div class="form-group">
          <label for="password_confirmation">Пароль</label>
          <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" required>
        </div>




        <div class="form-group">
          <button class="btn btn-lg btn-primary btn-block" type="submit">Регистрация</button>
        </div>
</form>
</div>
</div>
</div>
@endsection