<!-- resources/views/auth/password.blade.php -->
@extends('layouts.welcome')

@section('Title')
Отправка КП и прайсов с сайта на почту клиентов - Сервис от Agency911.org
@endsection

@section('content')

<div class="container-fluid">
  <div class="row">
    <div class="col-sm-10 col-sm-offset-1">
      <h2>Настройка проектов</h2>
      
@if (session('success'))
<div class="alert alert-success alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<strong>SUCCESS</strong> {{ session('success') }}
</div>
@endif

  
<div class="btn btn-primary" id="AddNewProjects" type="button">Добавить новый проект</div>
<div class="GolfField">
@if (isset($PPart))
<?php echo $PPart ?>
@endif
</div>

</div>
</div>
</div>
@include('modal_window') 
@include('modal_window2') 
@include('modal_window3') 
@endsection

@section('JS_CODE')
         {!! Html::script('js/JSproj.js') !!}
@endsection
