<!-- resources/views/auth/password.blade.php -->
@extends('layouts.welcome')

@section('Title')
Отправка КП и прайсов с сайта на почту клиентов - Сервис от Agency911.org
@endsection

@section('content')

<div class="container-fluid">
  <div class="row">
    <div class="col-sm-4 col-sm-offset-4">
      <h2>Настройка почты</h2>
      
@if (session('success'))

<div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>SUCCESS</strong> {{ session('success') }}
</div>


@endif

      <form class="form-horizontal" method="post" action="{{ url('/options/email') }}">
        {!! csrf_field() !!}
    <div class="form-group">
    <label for="SMTP_HOST" class="col-sm-4 control-label">HOST</label>
    <div class="col-sm-8">
    <input type="text" class="form-control" id="SMTP_HOST" placeholder="HostName" name="SMTP_HOST" value="{{$SMTP_HOST}}">
    </div>
    </div>


    <div class="form-group">
    <label for="SMTP_PORT" class="col-sm-4 control-label">Порт</label>
    <div class="col-sm-8">
    <input type="text" class="form-control" id="SMTP_PORT" placeholder="Порт" name="SMTP_PORT" value="{{$SMTP_PORT}}">
    </div>
    </div>


    <div class="form-group">
    <label for="SMTPAuth" class="col-sm-4 control-label">Авторизация</label>
    <div class="col-sm-8">
    <input type="text" class="form-control" id="SMTPAuth" placeholder="Порт" name="SMTPAuth" value="{{$SMTPAuth}}">
    </div>
    </div>

    <div class="form-group">
    <label for="SMTPSecure" class="col-sm-4 control-label">Secure</label>
    <div class="col-sm-8">
    <input type="text" class="form-control" id="SMTPSecure" placeholder="tls/ssl" name="SMTPSecure" value="{{$SMTPAuth}}">
    </div>
    </div>

    <div class="form-group">
    <label for="USERNAME_EMAIL" class="col-sm-4 control-label">Пользователь</label>
    <div class="col-sm-8">
    <input type="text" class="form-control" id="USERNAME_EMAIL" name="USERNAME" value="{{$USERNAME}}">
    </div>
    </div>

    <div class="form-group">
    <label for="PASSWORD_EMAIL" class="col-sm-4 control-label">Пароль</label>
    <div class="col-sm-8">
    <input type="password" class="form-control" id="PASSWORD_EMAIL" name="PASSWORD" value="{{$PASSWORD}}">
    </div>
    </div>


    <div class="form-group">
    <label for="SET_FROM_EMAIL" class="col-sm-4 control-label">From - Email</label>
    <div class="col-sm-8">
    <input type="text" class="form-control" id="SET_FROM_EMAIL" name="SET_FROM_EMAIL" value="{{$SET_FROM_EMAIL}}">
    </div>
    </div>

    <div class="form-group">
    <label for="SET_FROM_NAME" class="col-sm-4 control-label">From - NAME</label>
    <div class="col-sm-8">
    <input type="text" class="form-control" id="SET_FROM_NAME" name="SET_FROM_NAME" value="{{$SET_FROM_NAME}}">
    </div>
    </div>

    <div class="form-group">
    <label for="About" class="col-sm-4 control-label">About</label>
    <div class="col-sm-8">
    <textarea class="form-control" id="About" name="About" rows="3">{{$About}}</textarea>
    </div>
    </div>



          <button class="btn btn-lg btn-primary btn-block" type="submit">Сохранить</button>
        </div>


</form>

</div>
</div>
</div>
@endsection