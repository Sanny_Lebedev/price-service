<div class="panel panel-default" id="{{$Project_ID}}">
  <div class="panel-heading">
  <div class="top_panel_head">
    <div class="left_panel_head">      
    <div>№: {{$Project_ID}}</div>
    <div alt="{{$Project_NAME}}" id="proj_{{$Project_ID}}">Наименование:  {{$Project_NAME}} <button type="button" class="btn btn-default btn-xs glyphicon glyphicon-pencil RenameProject" id="{{$Project_ID}}"></button></div>
    <div>Идентификатор: {{$Project_UID}}</div>   

    </div>
    <div class="right_panel_head DelProjectBtn" id="{{$Project_ID}}">
    <div class="glyphicon glyphicon-trash"></div>
    
    </div>

  </div>
</div>

  <div class="panel-body">
          

  <button type="button" id="{{$Project_ID}}" class="btn btn-primary btn-sm glyphicon glyphicon-plus Add_Button"> Добавить </button>
  <button type="button" id="{{$Project_ID}}" class="btn btn-default btn-sm glyphicon glyphicon-user User_List"> Доступ </button>

        <table class="table table-hover">
            <tr>
                <th>№ кнопки</th>
                <th>Идентификатор</th>
                <th>Название кнопки</th>  
                <th>Действие</th>  
                
            </tr>
        @if (isset($Buttons))    
          @foreach ($Buttons as $Button)
              <tr>
                <td>{{$Button->id}}</td>
                <td>{{$Button->UID}}</td>
                <td>{{$Button->BTN_Caption}}</td>  
                <td>
                  @if ($ID_STATUS == 1) 
                  <a href="/options/btnedit/{{$Project_ID}}/{{$Button->id}}"><div class="glyphicon glyphicon-pencil BTNEDIT" id="{{$Project_ID}}_{{$Button->id}}" alt="Редактировать запись"></div></a>
                  <div class="glyphicon glyphicon-exclamation-sign BTNHOWTO" id="{{$Project_ID}}_{{$Button->id}}" alt="Как установить кнопку"></div>
                  <div class="glyphicon glyphicon-trash BTNDELETE" id="{{$Project_ID}}_{{$Button->id}}" alt="Редактировать запись"></div>

                  @endif
                </td>  
              </tr>
          @endforeach
         @endif 

        </table>

  </div>
</div>