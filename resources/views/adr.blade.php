<!-- resources/views/auth/password.blade.php -->
@extends('layouts.welcome')

        @section('DOP_CSS')  
        <link rel="stylesheet" href="/css/main.css">
        @endsection

@section('Title')
Отправка КП и прайсов с сайта на почту клиентов - Сервис от Agency911.org
@endsection



@section('content')

<div class="container-fluid">
  <div class="row">
    <div class="col-sm-10 col-sm-offset-1">
      <h1>Информация о проектах</h1>
      
@if (session('success'))
<div class="alert alert-success alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<strong>SUCCESS</strong> {{ session('success') }}
</div>
@endif

  @if (isset($StatEmail))
  <div class="proj_name"><h3>Проект: {{$Project_name}} (№ {{$Project_id}}) </h3></div>
  <table class="table table-hover table-striped">
  <tr>
  <th>Имя</th>
  <th>E-mail</th>
  <th>Обращений</th>
  </tr>
   @foreach ($StatEmail as $Stat)
   <tr>
   <td>{{$Stat->UserName}}</td>
   <td>{{$Stat->Mail}}</td>
   <td>{{$Stat->Count}}</td>
   </tr>
   @endforeach
   </table>
   {!! $StatEmail->render() !!}
  @endif


</div>
</div>
</div>

@endsection

@section('JS_CODE')
         
@endsection
