<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::get('/', 'OptionController@mainpart');

Route::get('/stat/{Project_ID}', ['middleware' => 'auth', 'uses' => 'OptionController@statist']);
Route::get('/adr/{Project_ID}', ['middleware' => 'auth', 'uses' => 'OptionController@adrlist']);



Route::get('/getsendbtn', 'JQController@sendbtn');
Route::get('/getsendmail', 'JQController@sendmail');

Route::get('/userlist', ['middleware' => 'auth', 'uses' => 'OptionController@userlist']);
Route::get('/options/email', ['middleware' => 'auth', 'uses' => 'OptionController@emailconfig']);
Route::post('/options/email', ['middleware' => 'auth', 'uses' => 'OptionController@emailconfig_update']);

Route::get('/options/projects', ['middleware' => 'auth', 'uses' => 'OptionController@projectshow']);
Route::post('/options/projects', ['middleware' => 'auth', 'uses' => 'OptionController@projectshow_update']);

Route::get('/options/projectsetup', ['middleware' => 'auth', 'uses' => 'OptionController@project_new']);
Route::post('/options/projectsetup', ['middleware' => 'auth', 'uses' => 'OptionController@project_new']);

Route::get('/options/btnedit/{Project_ID}/{Button_ID}', ['middleware' => 'auth', 'uses' => 'OptionController@button_edit']);
Route::post('/options/btnedit/{Project_ID}/{Button_ID}', ['middleware' => 'auth', 'uses' => 'OptionController@button_edit_save']);

Route::get('/otions/jqupdate', 
                         array('before' => 'csrf', 
                                 'uses' => 'OptionController@Get_new_projectname'));
Route::get('/otions/jqdelete', 
                         array('before' => 'csrf', 
                                 'uses' => 'OptionController@DelProject'));

Route::get('/otions/jqdelfile', 
                         array('before' => 'csrf', 
                                 'uses' => 'OptionController@Del_BTN_File'));

Route::get('/otions/jqdelBTN', 
                         array('before' => 'csrf', 
                                 'uses' => 'OptionController@Del_BTN_ALL'));

Route::get('/otions/jqADDBTN', 
                         array('before' => 'csrf', 
                                 'uses' => 'OptionController@ADD_BTN'));

Route::get('/otions/getprojusers', 
                         array('before' => 'csrf', 
                                 'uses' => 'JQController@GetUsersForProject'));


Route::get('/otions/gethowto', 
                         array('before' => 'csrf', 
                                 'uses' => 'JQController@HowToForButton'));

Route::get('/otions/addprojusers', 
                         array('before' => 'csrf', 
                                 'uses' => 'JQController@AddUsersForProject'));

Route::get('/otions/delprojusers', 
                         array('before' => 'csrf', 
                                 'uses' => 'JQController@DelUsersForProject'));

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

// Роуты запроса ссылки для сброса пароля
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Роуты сброса пароля
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

