<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use PHPMailer;
use Input;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Models\Servdatas;
use App\Models\Servact;
use App\Models\Servdecore;
use App\Models\EmailConf;
use App\Models\BtnFiles;
use App\Models\StatEmails;
use App\Models\StatSends;
use App\Models\User_Proj;


class JQController extends Controller
{

    


    protected function AddUsersForProject (Request $request)
    {
        $proj   =   $request->project;
        $mail   =   $request->email;
        $authid =   Auth::user()->id;
        // Проверяем наличие прав на проект у пользователя
        $MyUser = User_Proj::where("ID_PROJECT", $proj)
                           ->where("ID_USER", $authid)
                           ->first();
        if ($MyUser != NULL) {
            $GetNew = DB::table("users")
                        ->where("email","=", $mail)
                        ->first();

            

            if ($GetNew != NULL){
                        $GetOldFromList =  User_Proj::where("ID_PROJECT", $proj)
                                        ->where("ID_USER", $GetNew->id)
                                        ->first();
                if ($GetOldFromList == NULL){
                        $NewUserToList = new User_Proj;
                        $NewUserToList->ID_PROJECT  =   $proj;
                        $NewUserToList->ID_USER     =   $GetNew->id;
                        $NewUserToList->ID_STATUS   =   "1";
                        $NewUserToList->save();    
                }


            
            }
        }

        // Вытаскиваем список пользователей
        
        $User_Proj_List = DB::table("users")
                            ->join("User_Proj",  function ($join)  use ($proj){
                                $join->on('users.id', '=', 'User_Proj.ID_USER')
                                ->where('User_Proj.ID_PROJECT', '=', $proj);
                            })->get();
        $UOUR = view("layouts.Windows_userlist",[
            "User_Proj_List" => $User_Proj_List,
            "authid"         => $authid,
            ])->render();
        return $UOUR;                     
    }

    protected function DelUsersForProject (Request $request)
    {
        $authid =   Auth::user()->id;
        $proj   =   $request->project;
        $user   =   $request->user;
        
        // Проверяем наличие прав на проект у пользователя
        $MyUser = User_Proj::where("ID_PROJECT", $proj)
                           ->where("ID_USER", $authid)
                           ->first();
        if ($MyUser != NULL) {
            $DelMyUser = User_Proj::where("id",$user)->delete();
        }
         $User_Proj_List = DB::table("users")
                            ->join("User_Proj",  function ($join)  use ($proj){
                                $join->on('users.id', '=', 'User_Proj.ID_USER')
                                ->where('User_Proj.ID_PROJECT', '=', $proj);
                            })->get();
        $UOUR = view("layouts.Windows_userlist",[
            "User_Proj_List" => $User_Proj_List,
            "authid"         => $authid,
            ])->render();
        return $UOUR;             


    }

    protected function HowToForButton (Request $request)
    { 
        $authid =   Auth::user()->id;
        $proj   =   $request->project;
        $PF     =   explode("_", $proj);
        $Servdatas  =   Servdatas::where("id", $PF[0])->first();
        $Servdecore =   Servdecore::where("id", $PF[1])
                                  ->where("ID_PROJECT", $PF[0])
                                  ->first();
        $LineContr = '<div class="SendPriceBtn" id="'.$Servdatas->UID.'_'.$Servdecore->UID.'" style="float: left;">';
        $OUT = view("Windows.howto",[
            "PROJECT_UID"   =>  $Servdatas->UID,
            "BUTTON_UID"    =>  $Servdecore->UID,
            "LineContr"     =>  $LineContr,
            ])->render();
        return $OUT;

    }


    protected function GetUsersForProject (Request $request)
    {
// Вытаскиваем список пользователей
        $authid =   Auth::user()->id;
        $proj   =   $request->project;

        $User_Proj_List = DB::table("users")
                            ->join("User_Proj",  function ($join)  use ($proj){
                                $join->on('users.id', '=', 'User_Proj.ID_USER')
                                ->where('User_Proj.ID_PROJECT', '=', $proj);
                            })->get();
        $UOUR = view("layouts.Windows_userlist",[
            "User_Proj_List" => $User_Proj_List,
            "authid"         => $authid,
            ])->render();
        return $UOUR;                     
    }


    protected function sendbtn(Request $request)
    {

    $request->input('BtnCode');
	$SERV_DATAS  = Servdatas::where('UID', $request->input('ClientCode'))->first();
	$SERV_DECORE = Servdecore::where('UID', $request->input('BtnCode'))
							 ->where('id',$SERV_DATAS->id)
							 ->first();

	$BTN_OUT = view('emails.btn', 
        ['STYLE'        => $SERV_DECORE->BTN_Style, 
         'BTN_ID'       => $SERV_DECORE->UID, 
         'BTN_Caption'  => $SERV_DECORE->BTN_Caption, 
        ])->render();

	$Modal_OUT = view('emails.modal', 
        ['STYLE'        => $SERV_DECORE->Model_Style, 
         'Caption'      => $SERV_DECORE->Model_Caption,
         'BTN_ID'       => $SERV_DECORE->UID, 
        ])->render();

return response()->json(['name' => $request->input('ClientCode'), 'state' => 'CA', 'html' => $BTN_OUT, 'htmlModal' => $Modal_OUT])
                 ->setCallback($request->input('callback'));
       
    }

    protected function sendmail(Request $request)
    {

    $BTN     =   $request->input('BtnCode');
    $Project =   $request->input('ClientCode');
    $Mailto  =   $request->input('ClientEmail');
    $ClientName  =   $request->input('ClientName');

    $Servdatas = Servdatas::where("UID", $Project)
                          ->first();

    if ($Servdatas != NULL) { 

        $MailConf = EmailConf::where("ID_USER", $Servdatas->UserName)
                             ->first();
        if ($MailConf != NULL) {                     

            $Servdecore = Servdecore::where("UID", $BTN)->first();
            $Servact    = Servact::where("ID_USER", $Servdecore->id)->first();

    $mail = new PHPMailer(true);
    try {
        $mail->smtpConnect(
    array(
      "ssl" => array(
            "verify_peer" => false,
            "verify_peer_name" => false,
            "allow_self_signed" => true
        )
    )
);



// Сохраним статистику
        $StatSends = new StatSends;
        $StatSends->ID_PROJECT  =   $Servdatas->id;
        $StatSends->ID_BUTTON   =   $Servdecore->id;
        $StatSends->UserName    =   $ClientName;
        $StatSends->Email       =   $Mailto;
        $StatSends->save();

        $StatEmails = StatEmails::where("Mail", $Mailto)
                                ->where("OTHER", $Servdatas->id)
                                ->first();
        if ($StatEmails != NULL) {
                $StatEmails->Count = $StatEmails->Count+1;
            } else {
                $StatEmails=new StatEmails;
                $StatEmails->Count      = 1;
                $StatEmails->Mail       = $Mailto;
                $StatEmails->UserName   = $ClientName;
                $StatEmails->OTHER      = $Servdatas->id;
            }
        $StatEmails->save();    
// Отправим почту
        $mail->SMTPDebug = 0;
        $mail->isSMTP(); // tell to use smtp
        $mail->CharSet = "utf-8"; // set charset to utf8
        $mail->SMTPAuth = $MailConf->SMTPAuth;  // use smpt auth
        $mail->SMTPSecure = $MailConf->SMTPSecure; // or ssl
        $mail->Host = $MailConf->SMTP_HOST;
        $mail->Port = $MailConf->PORT; // most likely something different for you. This is the mailtrap.io port i use for testing. 
        $mail->Username = $MailConf->USERNAME;
        $mail->Password = $MailConf->PASSWORD;
        $mail->setFrom($MailConf->SET_FROM_EMAIL, $MailConf->SET_FROM_NAME);
        $mail->Subject = $Servact->LETTER_Caption;
        $mail->MsgHTML($Servact->LETTER_BODY);
        $mail->addAddress($Mailto, $Mailto);

        $path = base_path().'/public/FS/'.$Servdatas->id.'/'.$Servdecore->id.'/';

        $Files = BtnFiles::where("ID_PROJECT", $Servdatas->id)
                         ->where("ID_BUTTON", $Servdecore->id)
                         ->get();
        foreach ($Files as $File) {
                        if (file_exists($path.$File->FILENAME)) {
                            $mail->addAttachment($path.$File->FILENAME);                     
                            }
                         }                       

        $mail->send();
    } catch (phpmailerException $e) {
        dd($e);
    } catch (Exception $e) {
        dd($e);
    }
    die('success');
    return "YOU";
    }

    } 
    }

}
