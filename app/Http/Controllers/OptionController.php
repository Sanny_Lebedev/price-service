<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Form;
use HTML;
use Input;
use PHPMailer;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Redirect;
use App\Models\Servdatas;
use App\Models\Servact;
use App\Models\Servdecore;
use App\Models\EmailConf;
use App\Models\User_Proj;
use App\Models\BtnFiles;
use App\Models\StatSends;
use App\Models\StatEmails;

class OptionController extends Controller
{

    protected function adrlist(Request $request, $Project_ID)
    {
        // Проверяем наличие доступа у пользователя к проекту
        $User_Proj = User_Proj::where("ID_PROJECT", $Project_ID)
                              ->where("ID_USER", Auth::user()->id)
                              ->first();
        if ($User_Proj != NULL) {
            $Project   = Servdatas::where("id", $Project_ID)->first();   
            $StatEmail = StatEmails::where("OTHER", $Project_ID)
                                  ->paginate(25);

            return view('adr', [ 
                "StatEmail"     =>  $StatEmail, 
                "Project_id"    =>  $User_Proj->ID_PROJECT,
                "Project_name"  =>  $Project->About,
                ]); 
        } else {return redirect('/');}

    }

    protected function statist (Request $request, $Project_ID)
    {

        // Проверяем наличие доступа у пользователя к проекту
        $User_Proj = User_Proj::where("ID_PROJECT", $Project_ID)
                              ->where("ID_USER", Auth::user()->id)
                              ->first();
        $Project   = Servdatas::where("id", $Project_ID)->first();                      
        if ($User_Proj != NULL) {

            $StatSends_union = DB::table('StatSends')
                                 ->join('Servdecore', 'Servdecore.id', '=', 'StatSends.ID_BUTTON')
                                 ->where("StatSends.ID_PROJECT","=",$Project_ID)
                                 ->select("StatSends.created_at", "StatSends.UserName", "StatSends.Email", "Servdecore.BTN_Caption")
                                 ->paginate(25);



            return view('stat', [ 
                "StatSends"     =>  $StatSends_union, 
                "Project_id"    =>  $User_Proj->ID_PROJECT,
                "Project_name"  =>  $Project->About,
                ]);  



        } else { return redirect('/');}

    }

    protected function mainpart (Request $request)
    {
        if (Auth::check()) {
             $authid=Auth::user()->id; 
              $User_Proj_List = DB::table("Servdatas")
                            ->join("User_Proj",  function ($join)  use ($authid){
                                $join->on('User_Proj.ID_PROJECT', '=', 'Servdatas.id')
                                ->where('User_Proj.ID_USER', '=', $authid);
                            })->get();

                  return view('home', [ "Users_Proj_List"   =>  $User_Proj_List, ]);          
        } else {  return view('first'); }
    }


    protected function emailconfig_update(Request $request)
    {

        $authid=Auth::user()->id;
        $EmailConfig = EmailConf::where('ID_USER', $authid)->first();
        if ($EmailConfig == NULL) { $EmailConfig = new EmailConf; }

                $EmailConfig->ID_USER           = $authid;
                $EmailConfig->SMTP_HOST         = $request->SMTP_HOST;
                $EmailConfig->SMTPAuth          = $request->SMTPAuth;
                $EmailConfig->SMTPSecure        = $request->SMTPSecure;
                $EmailConfig->SMTP_PORT         = $request->SMTP_PORT;
                $EmailConfig->USERNAME          = $request->USERNAME;
                $EmailConfig->PASSWORD          = $request->PASSWORD;
                $EmailConfig->SET_FROM_EMAIL    = $request->SET_FROM_EMAIL;
                $EmailConfig->SET_FROM_NAME     = $request->SET_FROM_NAME;
                $EmailConfig->About             = $request->About;
                $EmailConfig->save();
                return Redirect::to('/options/email')->with('success', 'Настройки сохранены успешно');
    }

    protected function emailconfig(Request $request)
    {     

        $authid=Auth::user()->id;
        $EmailConfig = EmailConf::where('ID_USER', $authid)->first();
        if ($EmailConfig == NULL) {
                $EmailConfig = new EmailConf;
                $EmailConfig->SMTP_HOST         = '';
                $EmailConfig->SMTPAuth          = 'true';
                $EmailConfig->SMTPSecure        = 'tls';
                $EmailConfig->SMTP_PORT         = '25';
                $EmailConfig->USERNAME          = '';
                $EmailConfig->PASSWORD          = '';
                $EmailConfig->SET_FROM_EMAIL    = '';
                $EmailConfig->SET_FROM_NAME     = '';
                $EmailConfig->About             = '';
       
            }

        return view('EmailConfig',[
            'SMTP_HOST'         => $EmailConfig->SMTP_HOST,
            'SMTPAuth'          => $EmailConfig->SMTPAuth,
            'SMTPSecure'        => $EmailConfig->SMTPSecure,
            'SMTP_PORT'         => $EmailConfig->SMTP_PORT,
            'USERNAME'          => $EmailConfig->USERNAME,
            'PASSWORD'          => $EmailConfig->PASSWORD,
            'SET_FROM_EMAIL'    => $EmailConfig->SET_FROM_EMAIL,
            'SET_FROM_NAME'     => $EmailConfig->SET_FROM_NAME,
            'About'             => $EmailConfig->About
            ]);
    }



    protected function projectshow_update(Request $request)
    {
        $authid=Auth::user()->id;

        $USER_IN_LIST = User_Proj::where('ID_USER', $authid)->first();
        if($USER_IN_LIST == NULL) {
                $USER_IN_LIST = new User_Proj;
                $USER_IN_LIST->ID_USER = $authid;
                $USER_IN_LIST->ID_PROJECT = $proj;
                $USER_IN_LIST->save();
            }
    }
    

    //Добавим кнопку к проекту
    protected function ADD_BTN (Request $request)
    {
        $UID = str_random(12);
        $authid=Auth::user()->id; 
        $USER_IN_LIST = User_Proj::where("ID_PROJECT", $request->PRJ)
                                 ->where("ID_USER", $authid)
                                 ->first();

        if (($request->STEP == "ADDBTN")and($USER_IN_LIST != NULL))
        {
        $Servdecore = new Servdecore;
        $Servdecore->UID        =   $UID;
        $Servdecore->ID_PROJECT =   $request->PRJ;
        $Servdecore->BTN_Type   =   "0";
        $Servdecore->Model_Type =   "0";
        $Servdecore->save();
        $Les = view("layouts.PROJ_BUT",[
            "Servdecore"    =>  $Servdecore,
            "Project_ID"    =>  $request->PRJ,
            "ID_STATUS"     =>  $USER_IN_LIST->ID_STATUS,
            ])->render();
        return $Les;
        }
      
    }



    protected function Del_BTN_ALL (Request $request) {
        $authid=Auth::user()->id;
        $Param  =   explode("_", $request->BTN);
        $USER_IN_LIST = User_Proj::where("ID_PROJECT", $Param[0])
                                 ->where("ID_USER", $authid)
                                 ->first();
            if (($request->STEP == "DelBTN")and($USER_IN_LIST != NULL)) {
                // Удаляем файлы
                $path = '/public/FS/';
                $BFiles     =   BtnFiles::where("ID_PROJECT", $Param[0])
                                        ->where("ID_BUTTON", $Param[1])
                                        ->get();
                foreach ($BFiles as $File) {
                    if (file_exists(base_path().$path.$Param[0].'/'.$Param[1].'/'.$File->FILENAME)) {
                    unlink(base_path().$path.$Param[0].'/'.$Param[1].'/'.$File->FILENAME);
                    }                    
                }
                $BFiles     =   BtnFiles::where("ID_PROJECT", $Param[0])
                                        ->where("ID_BUTTON", $Param[1])
                                        ->delete();
                // Удаляем Коды кнопки
                $Servdecore = Servdecore::where("ID_PROJECT", $Param[0])
                                        ->where("id", $Param[1])
                                        ->delete();
                //Удаляем описание
                $Servact    = Servact::where("ID_USER", $Param[1])
                                     ->delete();
                if (is_dir(base_path().$path.$Param[0].'/'.$Param[1])){rmdir(base_path().$path.$Param[0].'/'.$Param[1]);}
                return "OK";


        }
    }

    protected function Del_BTN_File (Request $request)
    {
        
        $authid =   Auth::user()->id; 
        $Param  =   explode("_#_", $request->BTN);
        $USER_IN_LIST = User_Proj::where("ID_PROJECT", $Param[0])
                                 ->where("ID_USER", $authid)
                                 ->first();

        if (($request->STEP == "DelBTNFile")and($USER_IN_LIST != NULL)) {
 
                $path = '/public/FS/';
                if (file_exists(base_path().$path.$Param[0].'/'.$Param[1].'/'.$Param[2])) {
                    unlink(base_path().$path.$Param[0].'/'.$Param[1].'/'.$Param[2]);
                }
                $BFILE = BtnFiles::where("ID_PROJECT", $Param[0])
                                 ->where("ID_BUTTON", $Param[1])
                                 ->where("FILENAME", $Param[2])
                                 ->delete();
                return "OK";

        } else {
            return "STOP";
        }
    }

// Сохранение кнопок
    protected function button_edit_save(Request $request, $Project_ID, $Button_ID)
    {
        $path = '/public/FS/';
        $UID = str_random(12);
        $authid=Auth::user()->id; 
        $User_Proj = User_Proj::where("ID_USER", $authid)
                              ->where("ID_PROJECT", $Project_ID)
                              ->first();
        if ($User_Proj != NULL) {
            $Button = Servdecore::where("ID_PROJECT", $Project_ID)
                                ->where("id", $Button_ID)
                                ->first();
            if ($Button == NULL) {$Button = new Servdecore; 
                                  $Button->UID  =  $UID;}

            $Button->ID_USER          =  $authid;
            $Button->ID_PROJECT       =  $Project_ID;
            $Button->BTN_Type         =  "0";
            $Button->BTN_Text         =  "";
            $Button->BTN_Caption      =  $request->BTN_Caption;
            $Button->BTN_Style        =  $request->BTN_Style;
            $Button->Model_Type       =  "0";
            $Button->Model_Text       =  "";       
            $Button->Model_Caption    =  $request->Model_Caption;
            $Button->Model_Style      =  $request->Model_Style;
            $Button->save();

            // В данной таблице в поле ID_USER храним идентификатор кнопки!
            $Actions = Servact::where("ID_USER", $Button_ID)
                              ->first();
            $Actions->LETTER_Caption    = $request->LETTER_Caption;
            $Actions->LETTER_BODY       = $request->LETTER_BODY;
            $Actions->SMS_TEXT          = $request->SMS_TEXT;
            $Actions->save();

            // Работаем с файлами
            // Проверяем рабочие каталоги для проекта   
            if (!is_dir(base_path().$path.$Project_ID)) {
                mkdir(base_path().$path.$Project_ID, 0777);
                mkdir(base_path().$path.$Project_ID.'/'.$Button_ID, 0777);
            }  else {
                if (!is_dir(base_path().$path.$Project_ID.'/'.$Button_ID)) { mkdir(base_path().$path.$Project_ID.'/'.$Button_ID, 0777);}
            }   

            // Начинаем обрабатывать файлы                   
                foreach ($request->file('File') as $File)            
                {
                    
                if(!empty($File)){
                $filename=$File->getClientOriginalName();
                if (file_exists(base_path().$path.$Project_ID.'/'.$Button_ID.'/'.$filename))
                    {
                        unlink(base_path().$path.$Project_ID.'/'.$Button_ID.'/'.$filename); 
                        $BFILE = BtnFiles::where("ID_PROJECT", $Project_ID)
                                         ->where("ID_BUTTON", $Button_ID)
                                         ->where("FILENAME", $filename)
                                         ->delete();
                    }
                    $File->move(
                        base_path().$path.$Project_ID.'/'.$Button_ID.'/', $filename
                    );
                         // Добавляем данные в таблицу   
                        $BFILE = new BtnFiles;
                        $BFILE->ID_PROJECT  =   $Project_ID;
                        $BFILE->ID_BUTTON   =   $Button_ID;
                        $BFILE->FILENAME    =   $filename;
                        $BFILE->save();
                    }
                }
        }
        return redirect('/options/projects');
    }

// Редактирование кнопочки
    protected function button_edit(Request $request, $Project_ID, $Button_ID)
    {
        $authid=Auth::user()->id;
        
        $User_Proj = User_Proj::where("ID_USER", $authid)
                              ->where("ID_PROJECT", $Project_ID)
                              ->first();
        if ($User_Proj != NULL) {

            $Button = Servdecore::where("ID_PROJECT", $Project_ID)
                                ->where("id", $Button_ID)
                                ->first();
            if ($Button == NULL) {$Button = new Servdecore;}


            // В данной таблице в поле ID_USER храним идентификатор кнопки!
            $Actions = Servact::where("ID_USER", $Button_ID)
                              ->first();
            if ($Actions == NULL) {
                $Actions = new Servact;
                $Actions->ID_USER = $Button_ID;
                $Actions->save();
            }

            
            $BFILES = BtnFiles::where("ID_PROJECT", $Project_ID)
                             ->where("ID_BUTTON", $Button_ID)
                             ->get();
            if ($BFILES == NULL) {$BFILES = new BtnFiles;}


            return view("button",[
                "Button"        =>  $Button,
                "Actions"       =>  $Actions,
                "BFILES"        =>  $BFILES,
                "ID_PROJECT"    =>  $Project_ID,
                "ID_BUT"        =>  $Button_ID,
                ]);


        }   else {
            return redirect('/');
        }                   
    }

// Отображение списка проектов
    protected function projectshow(Request $request)
    {
        $authid=Auth::user()->id;

        $USER_IN_LIST = User_Proj::where('ID_USER', $authid)->get();
        if($USER_IN_LIST == NULL) {
            }
        else{
            $PPart="";
            foreach ($USER_IN_LIST as $User) {
                $GetProject = Servdatas::where('id', $User->ID_PROJECT)->first();
                $GetButtons = Servdecore::where('ID_PROJECT', $User->ID_PROJECT)->get();

                $PPart = $PPart.view('layouts.ProjectPart',[
                        "Project_ID"    =>  $GetProject->id,
                        "Project_NAME"  =>  $GetProject->About,
                        "Project_UID"   =>  $GetProject->UID,
                        "Buttons"       =>  $GetButtons,
                        "ID_STATUS"     =>  $User->ID_STATUS,

                    ])->render();
            }


        }

        $ModalWindow = view('Windows.newptoject')->render();


        return view('project',[
            'BODY_WINDOWS' => $ModalWindow,
            'TITLE_WINDOWS' => 'Создать проект',
            'PPart' => $PPart,
            ]);


    }

// Удаление проекта
    protected function DelProject(Request $request) {
        $authid =   Auth::user()->id;
        $OUT    =   "ERROR";
        if ($request->STEP=='DelProject'){ 
            $User_Proj  = User_Proj::where("ID_USER", $authid)
                                   ->where("ID_PROJECT", $request->ID_PROJ)
                                   ->first();
            if ($User_Proj != NULL) {                      
            $Project    = Servdatas::where("id",$request->ID_PROJ)
                                   ->delete();
            $US_PR      =  User_Proj::where("ID_PROJECT", $request->ID_PROJ)->delete(); 
            $Decore     = Servdecore::where("ID_PROJECT", $request->ID_PROJ)->get();
            foreach ($Decore as $Dec) {
                //Удаляем описание
                $Servact    = Servact::where("ID_USER", $Dec->id)
                                     ->delete();                
            }
            $Decore     = Servdecore::where("ID_PROJECT", $request->ID_PROJ)->delete();


                 // Удаляем файлы
                $path = '/public/FS/';
                $BFiles     =   BtnFiles::where("ID_PROJECT", $request->ID_PROJ)
                                        ->get();
                foreach ($BFiles as $File) {
                    if (file_exists(base_path().$path.$File->ID_PROJECT.'/'.$File->ID_BUTTON.'/'.$File->FILENAME)) {
                    unlink(base_path().$path.$File->ID_PROJECT.'/'.$File->ID_BUTTON.'/'.$File->FILENAME);
                    }
                
                }
                if (is_dir(base_path().$path.$request->ID_PROJ))
                    {
                        $dir    =   base_path().$path.$request->ID_PROJ;
                        $this->rrmdir($dir);
                    }
                $BFiles     =   BtnFiles::where("ID_PROJECT", $request->ID_PROJ)
                                        ->delete();


                


            $OUT="OK";
                            }
        }
    return $OUT;    
    }

protected function rrmdir($dir) { 
   if (is_dir($dir)) { 
     $objects = scandir($dir); 
     foreach ($objects as $object) { 
       if ($object != "." && $object != "..") { 
         if (filetype($dir."/".$object) == "dir") $this->rrmdir($dir."/".$object); else unlink($dir."/".$object); 
       } 
     } 
     reset($objects); 
     rmdir($dir); 
   } 
 } 


// Создание нового проекта
        protected function get_new_projectname(Request $request)
    {
        $authid=Auth::user()->id;

        if ($request->STEP=='NewProject'){
            
            if ($request->ID_PROJ != 0) {
                $SD = Servdatas::where('id', $request->ID_PROJ)->first();
            } else
            {
                $SD = new Servdatas;
                $UID = str_random(12);
                $SD->UID        = $UID;
                $SD->SMS_PROXY  = '0';
            }
            
            
            
            $SD->About    = $request->NameProject;
            $SD->UserName = $authid;
            $SD->save();
     
            if ($request->ID_PROJ == 0) {
            $USER_IN_LIST = new User_Proj;
            $USER_IN_LIST->ID_PROJECT = $SD->id;
            $USER_IN_LIST->ID_USER    = $authid;
            $USER_IN_LIST->ID_STATUS  = "1";
            $USER_IN_LIST->save();
            
           return view('layouts.ProjectPart',[
                "Project_ID"    =>  $SD->id,
                "Project_NAME"  =>  $request->NameProject,
                "Project_UID"   =>  $SD->UID,
                ]);
            } else {
                return $request->NameProject;
            }
            

        }
        
    }


}
