<?php 
//namespace App\Models;
namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Servdecore extends Eloquent {

  use SoftDeletes;

  protected $table = 'Servdecore';
  protected $dates = ['deleted_at'];
  protected $fillable = [
          'UID',
          'ID_PROJECT',
          'ID_USER',
          'BTN_Type',
          'BTN_Text',
          'BTN_Caption',
          'BTN_Style',
          'Model_Type',
          'Model_Text',
          'Model_Caption',
          'Model_Style'
  				];
	

}
