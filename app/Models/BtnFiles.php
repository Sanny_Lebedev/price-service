<?php 
//namespace App\Models;
namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class BtnFiles extends Eloquent {

  protected $table = 'BtnFiles';
  protected $fillable = [
          'ID_USER',
          'ID_PROJECT',
          'ID_BUTTON',
          'FILENAME'
  				];
	

}
