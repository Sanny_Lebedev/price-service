<?php 
//namespace App\Models;
namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class StatSends extends Eloquent {

  protected $table = 'StatSends';
  protected $fillable = [
          'ID_PROJECT',
          'ID_BUTTON',
          'UserName',
          'Email',
          'userip',
  				];
	

}
