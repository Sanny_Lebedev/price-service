<?php 
//namespace App\Models;
namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Servdatas extends Eloquent {

  use SoftDeletes;

  protected $table = 'Servdatas';
  protected $dates = ['deleted_at'];
  protected $fillable = [
          'email_out',
          'SMTP',
          'SMS_PROXY',
          'UserName',
          'Password',
          'PopAUTH',
          'UID',
          'About',
  				];
	

}
