<?php 
//namespace App\Models;
namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Servact extends Eloquent {

  use SoftDeletes;

  protected $table = 'Servact';
  protected $dates = ['deleted_at'];
  protected $fillable = [
          'UID',
          'ID_USER',
          'NAME_FILE',
          'LETTER_Caption',
          'LETTER_BODY',
          'About',
          'SMS_TEXT',
  				];
	

}
