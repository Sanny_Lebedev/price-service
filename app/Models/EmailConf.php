<?php 
//namespace App\Models;
namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmailConf extends Eloquent {

  use SoftDeletes;

  protected $table = 'EmailConf';
  protected $dates = ['deleted_at'];
  protected $fillable = [
          'ID_USER',
          'SMTP_HOST',
          'SMTPAuth',
          'SMTPSecure',
          'SMTP_PORT',
          'USERNAME',
          'PASSWORD',
          'SET_FROM_EMAIL',
          'SET_FROM_NAME',
          'About',
  				];
	

}
