<?php 
//namespace App\Models;
namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class User_Proj extends Eloquent {

  protected $table = 'User_Proj';
  protected $dates = ['deleted_at'];
  protected $fillable = [
          'ID_USER',
          'ID_PROJECT',
          'ID_STATUS',
  				];
	

}
