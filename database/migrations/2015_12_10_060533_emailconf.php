<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Emailconf extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('EmailConf', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ID_USER');
            $table->string('SMTP_HOST',50);
            $table->string('SMTPAuth',10);
            $table->string('SMTPSecure',10);
            $table->string('SMTP_PORT',10);
            $table->string('USERNAME',100);
            $table->string('PASSWORD',100);
            $table->string('SET_FROM_EMAIL',100);
            $table->string('SET_FROM_NAME',100);
            $table->string('About', 1000);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
            Schema::drop('EmailConf');
    }
}
