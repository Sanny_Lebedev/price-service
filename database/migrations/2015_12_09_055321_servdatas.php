<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Servdatas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Servdatas', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->string('email_out');
            $table->string('SMTP');
            $table->integer('SMS_PROXY');
            $table->string('UserName');
            $table->string('Password');
            $table->string('PopAUTH');
            $table->string('token');
            $table->string('UID');            
            $table->string('About');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('Servdatas');
    }
}
