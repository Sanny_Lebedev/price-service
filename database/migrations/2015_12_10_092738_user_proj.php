<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserProj extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('User_Proj', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ID_USER');
            $table->string('ID_PROJECT',50);
            $table->string('ID_STATUS',10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('User_Proj');
    }
}
