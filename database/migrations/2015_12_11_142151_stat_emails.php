<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Statemails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('Statemails', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Mail', 50);
            $table->string('UserName', 200);
            $table->integer('Count');
            $table->string('OTHER',150);
            $table->timestamps();
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Statemails');
    }
}
