<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BtnFiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('BtnFiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ID_USER');
            $table->string('ID_PROJECT',50);
            $table->string('ID_BUTTON',50);
            $table->string('FILENAME',200);
            $table->timestamps();
        });    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('BtnFiles');
    }
}
