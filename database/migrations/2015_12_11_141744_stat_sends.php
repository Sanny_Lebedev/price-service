<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StatSends extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('StatSends', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ID_PROJECT', 50);
            $table->string('ID_BUTTON',50);
            $table->string('UserName',150);
            $table->string('Mail',150);
            $table->string('userip',150);
            $table->timestamps();
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('StatSends');
    }
}
