<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Servdecore extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('Servdecore', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ID_PROJECT');
            $table->string('UID');
            $table->string('ID_USER');
            $table->integer('BTN_Type')->default(0);
            $table->longText('BTN_Text');
            $table->string('BTN_Caption', 50);
            $table->longText('BTN_Style');
            $table->integer('Model_Type')->default(0);
            $table->longText('Model_Text');
            $table->string('Model_Caption',50);
            $table->longText('Model_Style');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
                Schema::drop('Servdecore');
    }
}
