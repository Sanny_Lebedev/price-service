<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Servact extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('Servact', function (Blueprint $table) {
            $table->increments('id');
            $table->string('UID');
            $table->string('ID_USER');
            $table->string('NAME_FILE');
            $table->string('LETTER_Caption',100);
            $table->string('LETTER_BODY',2000);
            $table->string('About', 1000);
            $table->string('SMS_TEXT', 50);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('Servact');
    }
}
